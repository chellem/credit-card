<?php

require_once 'vendor/autoload.php';

use CC\Factory\Card\CreditCardFactory;
use CC\Factory\FileReader\FileReaderFactory;
use CC\Factory\Strategy\StrategyFactory;

use CC\Verification\CCVerification;

$verifier=new CCVerification();

// set the strategy and the chain of responsibility (can be in any order)
$verifier->setVerificationStrategy(StrategyFactory::getInstance()->create('visa'))
         ->getVerificationStrategy()
         ->setNext(StrategyFactory::getInstance()->create('MasterCard'))
         ->setNext(StrategyFactory::getInstance()->create('AmEx'))
         ->setNext(StrategyFactory::getInstance()->create('discover'))
         ;

$cards=array();
$invalidCards=array();

// can use resource/credit-card.json also
if ($fileReader=FileReaderFactory::getInstance()->create('resources/credit-card.csv')) {
    $message=null;
    foreach ($fileReader->getRecords() as $record) {
        try {
            $type=$verifier->setNumber($record->CardNumber)->verify();
            $card=CreditCardFactory::getInstance()->create($type);
            $card->setHolder($record->Holder)
                 ->setNumber($record->CardNumber)
                 ->setExpirationDate($record->ExpirationDate);
            //keep all valid card instance in array
            $cards[]=$card;
            $message=$card;
        } catch (\Exception $e) {
            $message="{$e->getMessage()}\n";
            // keep all invalid record in array
            $invalidCards[]=$record;
        }
        echo ($message);
    }
}