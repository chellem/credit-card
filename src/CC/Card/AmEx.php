<?php

namespace CC\Card;

class AmEx extends CreditCard
{
    public function pay($amount, $account)
    {
        throw new \Exception("AmericanExpress Payment not implement");
    }

    public function withdraw($amount)
    {
        throw new \Exception("AmericanExpress Withdrawal not implement");
    }

    public function getTransactionSummary()
    {
        throw new \Exception("AmericanExpress Transaction Summary not implement");
    }
}
