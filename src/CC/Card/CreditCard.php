<?php

namespace CC\Card;

abstract class CreditCard
{
    protected $number;
    protected $holder;
    protected $expirationDate;

    /**
     * Gets the value of number.
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Sets the value of number.
     *
     * @param mixed $number the number
     *
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Gets the value of holder.
     *
     * @return mixed
     */
    public function getHolder()
    {
        return $this->holder;
    }

    /**
     * Sets the value of holder.
     *
     * @param mixed $holder the holder
     *
     * @return self
     */
    public function setHolder($holder)
    {
        $this->holder = $holder;

        return $this;
    }

    /**
     * Gets the value of expirationDate.
     *
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Sets the value of expirationDate.
     *
     * @param mixed $expirationDate the expiration date
     *
     * @return self
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getType()
    {
        $items=explode('\\', get_class($this));

        return end($items);
    }

    public function __toString()
    {
        $message="The card is a {$this->getType()} with number {$this->getNumber()}";
        $message.=" belonging to {$this->getHolder()}, expiration date: {$this->getExpirationDate()}\n";

        return $message;
    }

    abstract public function pay($amount, $account);

    abstract public function withdraw($amount);

    abstract public function getTransactionSummary();

}
