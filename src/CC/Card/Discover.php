<?php

namespace CC\Card;

class Discover extends CreditCard
{
    public function pay($amount, $account)
    {
        throw new \Exception("Discover Payment not implement");
    }

    public function withdraw($amount)
    {
        throw new \Exception("Discover Withdrawal not implement");
    }

    public function getTransactionSummary()
    {
        throw new \Exception("Discover Transaction Summary not implement");
    }
}
