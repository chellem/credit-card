<?php

namespace CC\Card;

class MasterCard extends CreditCard
{
    public function pay($amount, $account)
    {
        throw new \Exception("MasterCard Payment not implement");
    }

    public function withdraw($amount)
    {
        throw new \Exception("MasterCard Withdrawal not implement");
    }

    public function getTransactionSummary()
    {
        throw new \Exception("MasterCard Transaction Summary not implement");
    }
}
