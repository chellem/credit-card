<?php

namespace CC\Card;

class Visa extends CreditCard
{
    public function pay($amount, $account)
    {
        throw new \Exception("Visa Payment not implement");
    }

    public function withdraw($amount)
    {
        throw new \Exception("Visa Withdrawal not implement");
    }

    public function getTransactionSummary()
    {
        throw new \Exception("Visa Transaction Summary not implement");
    }
}
