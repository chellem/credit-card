<?php

namespace CC\Factory\Card;

use CC\Factory\Creator;

class CreditCardFactory extends Creator
{
    protected static $instance;

    private function __construct() { }
    private function __clone() { }

    public function create($type)
    {
        $this->class = str_replace('Factory\\', null, __NAMESPACE__) . '\\' . ucfirst($type);

        return parent::getClassInstance();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new CreditCardFactory();
        }

        return self::$instance;
    }
}
