<?php

namespace CC\Factory;

abstract class Creator
{

    protected $class;

    abstract public function create($type);

    protected function getClassInstance($parameters = null)
    {
        if (class_exists($this->class, true)) {
            $reflectionClass = new \reflectionClass($this->class);
            if ($reflectionClass->isInstantiable()) {
                return new $this->class($parameters);
            }
        }
        throw new \Exception("Class {$this->class} not found or is not concrete");
    }
}
