<?php

namespace CC\Factory\FileReader;

use CC\Factory\Creator;

class FileReaderFactory extends Creator
{
    protected static $instance;

    private function __construct() { }
    private function __clone() { }

    public function create($file)
    {
        $items=explode('.', $file);
        $type=end($items);
        $this->class = str_replace('\\Factory', null, __NAMESPACE__) . '\\' . ucfirst($type).'FileReader';

        return parent::getClassInstance($file);
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new FileReaderFactory();
        }

        return self::$instance;
    }
}
