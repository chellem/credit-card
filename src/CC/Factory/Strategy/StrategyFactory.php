<?php

namespace CC\Factory\Strategy;

use CC\Factory\Creator;

class StrategyFactory extends Creator
{
    protected static $instance;

    private function __construct() { }
    private function __clone() { }

    public function create($type)
    {
        $this->class = str_replace('Factory', 'Verification', __NAMESPACE__) . '\\' . ucfirst($type).'Strategy';

        return parent::getClassInstance();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new StrategyFactory();
        }

        return self::$instance;
    }
}
