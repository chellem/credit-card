<?php

namespace CC\FileReader;

class CsvFileReader extends FileReader
{
    public function getRecords()
    {
        $count=0;
        $records = array();
        $handle = fopen($this->file, "r");
        if ($handle) {
            $headers=array();
            while (($line = fgets($handle)) !== false) {
                if ($count==0) {
                    $headers=$this->getFields($line);
                }
                $records[]=$this->createRecord($headers, $line);
                ++$count;
            }
        }
        fclose($handle);

        return $records;
    }

    private function createRecord($headers, $line)
    {
        $record=new \stdClass();
        foreach ($this->getFields($line) as $key => $field) {
            if (isset($headers[$key])) {
                $property=str_replace(PHP_EOL, null,$headers[$key]);
                $record->$property=$field;
            }
        }

        return $record;
    }

    private function getFields($line)
    {
        return explode(',',$line);
    }
}
