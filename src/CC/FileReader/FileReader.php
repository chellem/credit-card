<?php

namespace CC\FileReader;

abstract class FileReader
{
    protected $file;

    public function __construct($file)
    {
        if (!file_exists($file)) {
            throw new \Exception("File not found");
        }
        $this->file=$file;
    }

    abstract public function getRecords();
}
