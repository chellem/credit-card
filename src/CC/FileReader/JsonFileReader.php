<?php

namespace CC\FileReader;

class JsonFileReader extends FileReader
{
    public function getRecords()
    {
        return json_decode(file_get_contents($this->file));
    }
}
