<?php

namespace CC\Verification;

use CC\Verification\Strategy\VerificationStrategy;

class CCVerification
{
    protected $verificationStrategy;
    protected $number;

    public function setNumber($number)
    {
        $this->number=$number;

        return $this;
    }

    public function setVerificationStrategy(VerificationStrategy $verificationStrategy)
    {
        $this->verificationStrategy=$verificationStrategy;

        return $this;
    }

    public function getVerificationStrategy()
    {
        return $this->verificationStrategy;
    }

    public function verify()
    {
        if (!$this->verificationStrategy) {
            throw new \Exception("No verification strategy set");
        }

        return $this->verificationStrategy
                    ->setNumber($this->number)
                    ->handle();
    }
}
