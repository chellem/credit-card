<?php

namespace CC\Verification\Strategy;

class AmExStrategy extends VerificationStrategy
{
    protected function getExpression()
    {
        return '/^3(4|7)[0-9]{13}$/';
    }
}
