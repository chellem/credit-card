<?php

namespace CC\Verification\Strategy;

class DiscoverStrategy extends VerificationStrategy
{
    protected function getExpression()
    {
        return '/^6011[0-9]{12}$/';
    }
}
