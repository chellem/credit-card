<?php

namespace CC\Verification\Strategy;

class MasterCardStrategy extends VerificationStrategy
{
    protected function getExpression()
    {
        return '/^5[1-5][0-9]{14}$/';
    }
}
