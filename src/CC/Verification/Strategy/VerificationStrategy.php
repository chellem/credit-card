<?php

namespace CC\Verification\Strategy;

abstract class VerificationStrategy
{
    protected $number;
    private $next;

    public function setNumber($number)
    {
        $this->number=$number;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getType()
    {
        $className=str_replace(__NAMESPACE__.'\\', null, get_class($this));

        return preg_replace('/Strategy$/', null, $className);
    }

    public function verify()
    {
        if (preg_match($this->getExpression(), $this->getNumber())) {
            return true;
        }

        return false;
    }

    public function getNext()
    {
        return $this->next;
    }

    public function setNext(VerificationStrategy $next)
    {
        $this->next=$next;

        return $next;
    }

    public function hasNext()
    {
        return $this->next ? true : false;
    }

    public function handle()
    {
        if ($this->verify()) {
            return $this->getType();
        }
        if ($this->hasNext()) {
            return $this->next
                        ->setNumber($this->getNumber())
                        ->handle();
        }
        throw new \Exception("Failed to recognize credit card");
    }

    abstract protected function getExpression();
}
