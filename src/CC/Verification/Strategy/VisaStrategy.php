<?php

namespace CC\Verification\Strategy;

class VisaStrategy extends VerificationStrategy
{
    protected function getExpression()
    {
        return '/^4([0-9]{12}|[0-9]{15})$/';
    }
}
